#include <iostream>

using namespace std;

void regularTransaction(int x, int y); // initializes the function 

void hackedTransaction(int& x, int& y); // initializes the function with references

void main ()
{
	int myAccount = 0; // sets value
	int yourAccount = 999; // sets value

	cout << "Original Values\n\n";
	cout << "My balance: " << myAccount << "\n";
	cout << "Your balance: " << yourAccount << "\n\n";
	cout << "Checking balance...\n\n";
	regularTransaction(myAccount, yourAccount); // calls the function
	cout << "My balance: " << myAccount << "\n";
	cout << "Your balance: " << yourAccount << "\n\n";
	cout << "Hacking and checking balance again...\n\n";
	hackedTransaction(myAccount, yourAccount); // calls the function with references
	cout << "My balance: " << myAccount << "\n";
	cout << "Your balance: " << yourAccount << "\n\n";
	cout << "You should increase your security...\n";
	system("pause");
}

void regularTransaction(int x, int y)
{
	// only effects the copy of the int passed in
	int temp = x; // sets a temp value to x
	x = y; // sets x to y
	y = temp; // sets y to the temp value that was x
}

void hackedTransaction(int& x, int& y)
{
	// effects the actual numbers since it's a reference
	int temp = x; // sets a temp value to x
	x = y; // sets x to y
	y = temp; // sets y to the temp value that was x
}